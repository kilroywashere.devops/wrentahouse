import { Injectable } from '@angular/core';
import {Observable, of} from "rxjs";
import { tap } from 'rxjs/operators';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import jwt_decode, {JwtPayload} from "jwt-decode";
import {environment} from "../../environments/environment";


interface AuthenticationResponse {
  status: boolean;
  token: string;
  message: string;
}


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  redirectUrl = '/';

  private readonly authenticationUrl: string;

  constructor(private httpClient: HttpClient) {
    this.authenticationUrl = environment.authenticationApiURl;
  }

  static isLoggedIn() {
    // Checks if there is a saved token and it's still valid
    const token = AuthenticationService.getToken(); // Getting token from localstorage
    return !!token && !AuthenticationService.isTokenExpired(token);
  }

  static isTokenExpired(token: string) {
    /*
    TODO : faire l'authentification - 1
    try {
      const decoded: JwtPayload = jwt_decode(token);
      return decoded.exp === undefined ? false : decoded.exp < Date.now() / 1000;
    } catch (err) {
      return false;
    }
    */
    return false;
  }

  static setToken(idToken: string) {
    // Saves user token to sessionStorage
    sessionStorage.setItem('id_token', idToken);
  }

  static getToken() {
    // Retrieves the user token from sessionStorage
    return sessionStorage.getItem('id_token');
  }

  static logout() {
    // Clear user token and profile data from sessionStorage
    sessionStorage.removeItem('id_token');
  }

  /*static getProfile() {
    // Using jwt-decode npm package to decode the token
    return jwt_decode(AuthenticationService.getToken());
  }*/


  /*login(login: string, password: string): Observable<object> {
    return this.loginWithRole(login, password, null);
  }*/

  loginWithRole(username: string, password: string, role: string): Observable<AuthenticationResponse> {
    const url = `${this.authenticationUrl}/login`;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    /*
    TODO : faire l'authentification - 2
    return this.httpClient.request<AuthenticationResponse>('POST', url, {
      body: {
        username,
        password,
        role
      },
      headers: httpOptions.headers
    }).pipe(
      tap((data: AuthenticationResponse) => AuthenticationService.setToken(data.token)) // Setting the token in sessionStorage
    );
  }
  */
    const response: AuthenticationResponse = {status: true, message: 'HTTP 200', token: 'atoken'};
    AuthenticationService.setToken('atoken');

    return of(response);
  }
}
