import {inject} from "@angular/core";
import { Router } from "@angular/router";
import {AuthenticationService} from "./authentication.service";

export const AuthGuard = () => {
  // const auth = inject(AuthenticationService);
  const router = inject(Router);

  if(! AuthenticationService.isLoggedIn()) {
    router.navigateByUrl('/login').then(r => {
      console.error('Navigation impossible')
    });
    return false
  }
  return true
}
