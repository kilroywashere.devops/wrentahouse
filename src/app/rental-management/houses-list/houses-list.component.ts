import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {Habitation} from "../models/habitation";
import {MatPaginator} from "@angular/material/paginator";
import {Router} from "@angular/router";
import {HabitationsService} from "../services/habitations.service";
import {TypeHabitat} from "../models/type-habitat";
import {TypeHabitatsService} from "../services/type-habitats.service";

@Component({
  selector: 'app-houses-list',
  templateUrl: './houses-list.component.html',
  styleUrls: ['./houses-list.component.css']
})
export class HousesListComponent implements OnInit {
  displayedColumns: string[] = ['typehabitat', 'items', 'optionpayantes', 'libelle', 'description', 'image', 'prixnuit'];
  dataSource = new MatTableDataSource<Habitation>([]);

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator | null;
  typeHabitats: TypeHabitat[] = [];
  selectedTypeHabitat = "0";

  constructor(
    private habitationsService: HabitationsService,
    private typeHabitatsService: TypeHabitatsService,
    private router: Router) {
    this.paginator = null;
  }

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.filterPredicate = (data: Habitation, filter: string) => {
      return this.filterPredicate(data, filter);
    };

    this.getHabitations();
    this.getTypeHabitats();
  }
  applyFilter($event: KeyboardEvent) : void {
    const filterValue = ($event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  edit(id: number) : void {
    this.router.navigate(['/rent/housing/', id]).then( (e) => {
      if (!e) {
        console.error('HousesListComponent - Navigation has failed !');
      }
    });
  }

  addHabitation() {
    this.router.navigate(['/rent/housing/add']).then( (e) => {
      if (! e) {
        console.log('LdapListComponent - Navigation has failed!');
      }
    });
  }

  private getHabitations(id?: number) : void {
    this.habitationsService.getHabitations(id===undefined ? 0 : id!).subscribe(
      habitations => {
        this.dataSource.data = habitations;
      }
    );
  }

  private getTypeHabitats() {
    this.typeHabitatsService.getTypeHabitats().subscribe(
      typeHabitats => {
        this.typeHabitats = typeHabitats;
      }
    );
  }

  private filterPredicate(data: Habitation, filter: string) :boolean {
    return !filter || data.libelle.toLowerCase().startsWith(filter);
  }

  onTypeHabitatChange() {
    this.getHabitations(Number.parseInt(this.selectedTypeHabitat));
  }
}
