import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {AuthGuard} from "../security/auth.guard";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {HousesListComponent} from "./houses-list/houses-list.component";

const adminRoutes: Routes = [
  {
    path: 'rent',
    canActivate: [AuthGuard],
    children: [
      { path: '', component: DashboardComponent },
      { path: 'housing/list', component: HousesListComponent },
      { path: '', redirectTo: '/rent', pathMatch: 'full' },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(adminRoutes)],
  exports: [RouterModule]
})
export class RentalManagementRoutingModule { }
