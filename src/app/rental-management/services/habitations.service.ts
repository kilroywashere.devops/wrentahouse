import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Habitation} from "../models/habitation";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class HabitationsService {

  private habitationUrl = environment.habitationsApiUrl;
  private httpOptions = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(private http: HttpClient) { }

  getHabitations(id: number): Observable<Habitation[]> {
    if (id === undefined || id === 0) {
      return this.http.get<Habitation[]>(this.habitationUrl);
    }
    return this.http.get<Habitation[]>(`${this.habitationUrl}/typehabitat/${id}`);
  }

  getHabitation(id: number): Observable<Habitation> {
    return this.http.get<Habitation>(`${this.habitationUrl}/${id}`);
  }

  addHabitation(user: Habitation): Observable<Habitation> {
    return this.http.post<Habitation>(this.habitationUrl, user, {
      headers: this.httpOptions
    });
  }

  updateHabitation(userToUpdate: Habitation): Observable<Habitation> {
    // Modification de l'habitation
    return this.http.put<Habitation>(this.habitationUrl, userToUpdate, {
      headers: this.httpOptions
    });
  }

  deleteHabitation(id: number): Observable<Habitation> {
    return this.http.delete<Habitation>(`${this.habitationUrl}/${id}`, {
      headers: this.httpOptions
    });
  }
}
