import { TestBed } from '@angular/core/testing';

import { TypeHabitatsService } from './type-habitats.service';

describe('TypeHabitatsService', () => {
  let service: TypeHabitatsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TypeHabitatsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
