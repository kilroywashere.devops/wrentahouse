import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {TypeHabitat} from "../models/type-habitat";

@Injectable({
  providedIn: 'root'
})
export class TypeHabitatsService {

  private typeHabitatsUrl = environment.typeHabitatsUrl;
  private httpOptions = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(private http: HttpClient) { }

  getTypeHabitats(): Observable<TypeHabitat[]> {
    return this.http.get<TypeHabitat[]>(this.typeHabitatsUrl);
  }
}
