import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RentalManagementRoutingModule } from './rental-management-routing.module';
import {AppMaterialModule} from "../app-material.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {BrowserModule} from "@angular/platform-browser";
import { NavbarComponent } from './navbar/navbar.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HousesListComponent } from './houses-list/houses-list.component';
import {HttpClientModule} from "@angular/common/http";
import {OptionPayanteLocationComponent} from "./share/option-payante-location/option-payante-location.component";
import {TypeHabitatComponent} from "./share/type-habitat/type-habitat.component";
import {OptionsComponent} from "./share/options/options.component";


@NgModule({
  declarations: [
    NavbarComponent,
    DashboardComponent,
    HousesListComponent,
    OptionsComponent,
    OptionPayanteLocationComponent,
    TypeHabitatComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppMaterialModule,
    RentalManagementRoutingModule,
    HttpClientModule,
  ]
})
export class RentalManagementModule { }
