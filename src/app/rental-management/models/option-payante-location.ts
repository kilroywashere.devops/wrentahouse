import {Item} from "./item";

export interface OptionPayanteLocation {
  optionpayante: Item,
  prix: number,
}
