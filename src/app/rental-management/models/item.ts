
export interface Item {
  id: number;
  libelle: string,
  description: string,
}
