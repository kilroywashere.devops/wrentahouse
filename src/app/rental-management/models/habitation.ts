import {TypeHabitat} from "./type-habitat";
import {Item} from "./item";
import {OptionPayanteLocation} from "./option-payante-location";

export interface Habitation {
  id: number;
  typehabitat : TypeHabitat,
  items: Item[],
  images: string[],
  optionpayantes: OptionPayanteLocation[],
  libelle: string,
  description: string,
  adresse: string,
  codepostal: string,
  image: string,
  prixnuit: number,
}
