import {Component, Input} from '@angular/core';
import {OptionPayanteLocation} from "../../models/option-payante-location";

@Component({
  selector: 'app-options-payante-location',
  templateUrl: './option-payante-location.component.html',
  styleUrls: ['./option-payante-location.component.css']
})
export class OptionPayanteLocationComponent {
  @Input() optionpayantes!: OptionPayanteLocation[];
}
