import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OptionPayanteLocationComponent } from './option-payante-location.component';

describe('OptionPayanteLocationComponent', () => {
  let component: OptionPayanteLocationComponent;
  let fixture: ComponentFixture<OptionPayanteLocationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OptionPayanteLocationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OptionPayanteLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
