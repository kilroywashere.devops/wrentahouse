import {Component, Input} from '@angular/core';
import {Item} from "../../models/item";

@Component({
  selector: 'app-options',
  templateUrl: './options.component.html',
  styleUrls: ['./options.component.css']
})
export class OptionsComponent {
  @Input() options!: Item[];

}
