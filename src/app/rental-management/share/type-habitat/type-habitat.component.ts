import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-type-habitat',
  templateUrl: './type-habitat.component.html',
  styleUrls: ['./type-habitat.component.css']
})
export class TypeHabitatComponent {
  @Input() typeId: number;

  constructor() {
    this.typeId = 0;
  }


  getIcon() : string {
    return this.typeId===1 ? 'home' : 'apartment';
  }
}
